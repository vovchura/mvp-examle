package presenter;

import model.Model;
import view.IView;
import view.View;


public class Presenter implements IPresenter  {
   private Model model;
   private IView view;



    public Presenter(IView view) {
        this.view=view;
        this.model=new Model();
    }

    @Override
    public void getMassage() {
        view.showMassage(model.getString());
    }
}
