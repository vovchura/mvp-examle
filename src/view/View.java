package view;

import presenter.Presenter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class View extends JFrame implements IView {

    static JFrame jFrame;
    static JPanel jPanel;
    static String textPanel = " ";
    static JTextField jTextField;



    public static void main(String[] args) {
        View view =new View();
        Presenter presenter = new Presenter(view);

        jFrame = new JFrame("Practice");
        jPanel = new JPanel();
        jPanel.setLayout(null);
        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jFrame.setSize(400,200);
        jFrame.setLocationRelativeTo(null);
        jFrame.setResizable(false);
        jFrame.add(jPanel);

        jTextField = new JTextField();
        jTextField.setBounds(20,15, 360, 20);
        jTextField.setHorizontalAlignment(4);
        jPanel.add(jTextField);

        JButton btn =new JButton("Button");
        btn.setBounds(80,60,120,40);
        jPanel.add(btn);
        btn.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){

                presenter.getMassage();
            }
        });

        jFrame.revalidate();
        jFrame.setVisible(true);
        System.out.println(textPanel);
    }

    @Override
    public void showMassage(String massage) {
        jTextField.setText(massage);
    }

}
